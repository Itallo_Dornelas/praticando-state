import { Component } from "react";
import Container from "./components/Container/Container";
import Wrapper from "./components/Wrapper/Wrapper";
class App extends Component {
  state = {
    count: 0,
  };
  toAdd = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };
  remove = () => {
    this.setState({
      count: this.state.count - 1,
    });
  };

  render() {
    return (
      <Container>
        <h1>Contador</h1>
        <Wrapper className="wrapper">{this.state.count}</Wrapper>
        <Wrapper>
          <button className="toAdd" onClick={this.toAdd}>
            +1
          </button>
          <button className="remove" onClick={this.remove}>
            -1
          </button>
        </Wrapper>
      </Container>
    );
  }
}

export default App;
